package com.home.drawing.drawer;

import com.home.drawing.shape.Line;
import com.home.drawing.shape.Rectangle;
import com.home.drawing.shape.Shape;
import com.home.drawing.tools.Point;

import java.util.List;

/**
 * the Drawer class, used to draw shapes
 */
public class Drawer {

    /**
     * draw a rectangle
     *
     * @param points          the canvas
     * @param upLeft          the upper left coordinate
     * @param loRight         the lower right coordinate
     * @param rectangleBorder the shape border
     */
    public static void drawRectangle(List<Point> points,
                                     Point upLeft,
                                     Point loRight,
                                     RectangleBorder rectangleBorder) {
        Shape shape = Rectangle.builder()
                .canvas(points)
                .upLeft(upLeft)
                .loRight(loRight)
                .rectangleBorder(rectangleBorder)
                .build();
        shape.draw();
    }

    /**
     * draw a line
     *
     * @param points          the canvas
     * @param X1              a point
     * @param X2              a point
     * @param rectangleBorder the shape border
     */
    public static void drawLine(List<Point> points,
                                Point X1,
                                Point X2,
                                RectangleBorder rectangleBorder) {
        Shape shape = Line.builder()
                .canvas(points)
                .X1(X1)
                .X2(X2)
                .rectangleBorder(rectangleBorder)
                .build();
        shape.draw();
    }

}
