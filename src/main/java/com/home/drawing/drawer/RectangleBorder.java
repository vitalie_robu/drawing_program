package com.home.drawing.drawer;


public enum  RectangleBorder {

    CANVAS(" * "),
    RECTANGLE(" x ");

    String border;

    RectangleBorder(String border) {
        this.border = border;
    }

    public String getBorder() {
        return border;
    }
}