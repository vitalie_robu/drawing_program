package com.home.drawing.drawer;

import com.home.drawing.in.CanvasIn;
import com.home.drawing.in.UserOption;
import com.home.drawing.tools.Point;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class DrawOptionFactory {

    public static List<Point> execute(UserOption in, List<Point> points) {
        if(in != null) {
            switch (in) {
                case DRAW_CANVAS: {
                    if(points.isEmpty()) {
                        points = CanvasIn.drawCanvas(); break;
                    }
                    else {
                        System.out.println("The canvas was created, please take another action or clear to create a new canvas");
                        return points;
                    }
                }
                case DRAW_RECTANGLE: {
                    if(CanvasIn.drawRectangle(points).isEmpty()) {
                        return points;
                    }
                    break;
                }
                case DRAW_LINE: {
                    if(CanvasIn.drawLine(points).isEmpty()) {
                        return points;
                    }
                    break;
                }
                case QUIT:
                    System.exit(0);
                case CLEAR:
                    return execute(null, Collections.emptyList());
                default:
            }
        }
        draw(points);
        return points;

    }

    private static void draw(List<Point> points) {
        Map<Integer, List<Point>> grouped = points.stream().collect(groupingBy(Point::getY));
        grouped.forEach((k,v) -> System.out.println(v.stream().map(Point::getElement).collect(Collectors.joining())));
    }

}
