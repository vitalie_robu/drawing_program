package com.home.drawing.aspect;

import com.home.drawing.in.InputException;
import com.home.drawing.in.OptionIn;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import java.util.Collections;

@Aspect
public class ErrorAspect {

    /**
     * if any input exception is caught the application is reset
     */
    @Around("execution(* *(..)) && @annotation(com.home.drawing.aspect.ExecutionException)")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        Object proceed = null;
        try {
            proceed = pjp.proceed();
        }
        catch (InputException e) {
            System.out.println(e.getMessage());
            OptionIn.displayOptions(0, Collections.emptyList());
        }
        return proceed;
    }
}
