package com.home.drawing.in;

import java.util.Arrays;
public enum UserOption {

    DRAW_CANVAS(1, "Draw canvas"),
    DRAW_LINE(2, "Draw line"),
    DRAW_RECTANGLE(3, "Draw rectangle"),
    CLEAR(4, "Clear"),
    QUIT(5, "Quit");

    private int index;
    private String message;

    UserOption(int index, String message) {
        this.index = index;
        this.message = message;
    }

    public static UserOption getOptionById(int id) {
        return Arrays
                .stream(UserOption.values())
                .filter(userOption -> userOption.getIndex() == id)
                .findAny()
                .orElse(QUIT);
    }

    public int getIndex() {
        return index;
    }

    public String getMessage() {
        return message;
    }
}
