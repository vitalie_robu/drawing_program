package com.home.drawing.in;

import com.home.drawing.drawer.Drawer;
import com.home.drawing.drawer.RectangleBorder;
import com.home.drawing.tools.Canvas;
import com.home.drawing.tools.Point;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collections;
import java.util.List;

import static com.home.drawing.in.OptionIn.scan;

/**
 * Helper methods for getting inputs for drawer
 */
public interface CanvasIn {

    /**
     * Draw the canvas
     * @return the list of coordinates, mutable instance which will be used as coordinates and drawing 'sheet'
     */
    static List<Point> drawCanvas() {
        System.out.println("Enter Canvas dimensions");
        int width = scan("canvas", "width", 0);
        int high = scan("canvas", "high", 0);
        return Canvas.draw(width, high);
    }

    /**
     * get input for drawing a rectangle and calls the Drawer
     * a pair of points the upperLeft and lowerRight are fetched from System.in
     * @param points the coordinates 'sheet'
     * @return the coordinates 'sheet' or an empty collection if the input points are not valid
     */
    static List<Point> drawRectangle(List<Point> points) {
        if(points.isEmpty()) {
            return points;
        }
        System.out.println("Enter Rectangle dimensions");
        Pair<Point, Point> pointPair = getPoints( "upper left", "lower right");
        if(pointsAreValid(pointPair, points)) {
            Drawer.drawRectangle(points, pointPair.getKey(), pointPair.getValue(), RectangleBorder.RECTANGLE);
            return points;
        }
        System.out.println("Selected points are out of index!!! Try again!");
        return Collections.emptyList();
    }

    /**
     * get input for drawing a line and calls the Drawer
     * a pair of points the A and B are fetched from System.in
     * @param points the coordinates 'sheet'
     * @return the coordinates 'sheet' or an empty collection if the input points are not valid
     */
    static List<Point> drawLine(List<Point> points) {
        if(points.isEmpty()) {
            return points;
        }
        System.out.println("Enter line point A and B");
        Pair<Point, Point> pointPair = getPoints( "point A ", "point B");
        if(pointsAreValid(pointPair, points)) {
            Drawer.drawLine(points, pointPair.getKey(), pointPair.getValue(), RectangleBorder.RECTANGLE);
            return points;
        }
        System.out.println("Selected points are out of index!!! Try again!");
        return Collections.emptyList();
    }

    /**
     *
     * @param title the input title
     * @param step the input placeholder
     * @return the scanned points
     */
    static Pair<Point, Point> getPoints(String title, String step) {
        int ulX = scan(title, "X", 0);
        int ulY = scan(title, "Y", 0);
        int lrX = scan(step, "X", 0);
        int lrY = scan(step, "Y", 0);
        Point A = new Point(ulX, ulY);
        Point B = new Point(lrX, lrY);
        return Pair.of(A, B);
    }

    /**
     * validates the scanned points, if they are within 'sheet' index
     * @param pointPair the point pair
     * @param points the 'sheet'
     * @return if points are valid
     */
    static boolean pointsAreValid(Pair<Point, Point> pointPair, List<Point> points) {
        return points.indexOf(pointPair.getKey()) != -1 && points.indexOf(pointPair.getValue()) != -1;
    }

}
