package com.home.drawing.in;

import com.home.drawing.aspect.ExecutionException;
import com.home.drawing.drawer.DrawOptionFactory;
import com.home.drawing.tools.Point;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * The Menu Options
 */
public interface OptionIn {

    Scanner scanner = new Scanner(System.in);

    // the input max retry
    int MAX_RETRIES = 3;

    static void displayOptions(int retry, List<Point> points) {
        if(retry < MAX_RETRIES) {
            System.out.println("Please select one of the options:");
            List<Integer> options = new ArrayList<>();
            for (UserOption userOption : UserOption.values()) {
                System.out.println(String.format("\n%d . %s", userOption.getIndex(), userOption.getMessage()));
                options.add(userOption.getIndex());
            }
            int option = scan("option", options.stream().map(String::valueOf).collect(Collectors.joining(",")), 0);
            if (options.contains(option)) {
                List<Point> optionPoints = DrawOptionFactory.execute(UserOption.getOptionById(option), points);
                if(optionPoints.isEmpty()) {
                    System.out.println("No canvas is drawn yet, create a canvas first");
                    displayOptions(retry, optionPoints);
                }
                displayOptions(0, optionPoints);
            } else {
                displayOptions(retry, points);
            }
        }
    }

    /**
     * user input will be validated here
     * if retries are more than max retries an exception will be thrown
     * An Aspect will handle it and the Menu will be redrawn with mention about failure
     * @param title input title/label
     * @param step input placeholder
     * @param retry retry attempts
     * @return the option
     */
    @SneakyThrows
    @ExecutionException
    static int scan(String title, String step, int retry) {
        System.out.println("Enter " + title + " : " + step);
        if(retry <= MAX_RETRIES) {
            String width = scanner.next();
            if (isValid(step, width)) {
                return Integer.parseInt(width);
            }
            return scan(title, step, ++retry);
        }
        throw new InputException("Failed to get value for step %s" + step);
    }

    static boolean isValid(String step, String value) {
        if(!StringUtils.isNumeric(value)) {
            System.out.println(String.format("Entered amount should be of numeric type, resetting step %s", step));
            return false;
        }
        return true;
    }

}
