package com.home.drawing.tools;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = {"x", "y"})
public class Point {

    private int x;
    private int y;
    private String element = "   ";
    private int index;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
