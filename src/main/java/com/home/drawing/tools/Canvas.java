package com.home.drawing.tools;

import com.home.drawing.drawer.Drawer;
import com.home.drawing.drawer.RectangleBorder;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
/*
    Canvas util
 */
public interface Canvas {

    static List<Point> draw(int width, int height) {
        //generate the empty 'sheet'
        List<Point> points = generateCanvasPoints(width, height);
        //draw the canvas/borders
        Drawer.drawRectangle(points, new Point(0, 0), new Point(width - 1, height - 1), RectangleBorder.CANVAS);
        return points;
    }

    /**
     * this generates the canvas point used further for as mutable for holding the drawings aka 'sheet'
     * @param width the 'sheet' width
     * @param height the 'sheet' height
     * @return the points (coordinates)
     */
    static List<Point> generateCanvasPoints(int width, int height) {
        return IntStream.range(0, height)
                .mapToObj(y -> IntStream.range(0, width)
                                .mapToObj(x -> new Point(x, y))
                )
                .flatMap(Function.identity())
                .collect(Collectors.toList());
    }
}
