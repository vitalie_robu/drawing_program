package com.home.drawing.shape;

@FunctionalInterface
public interface Shape {
    void draw();
}
