package com.home.drawing.shape;

import com.home.drawing.drawer.RectangleBorder;
import com.home.drawing.tools.Point;
import lombok.Builder;

import java.util.List;

import static com.home.drawing.drawer.Drawer.drawLine;

@Builder
public class Line implements Shape {

    private List<Point> canvas;
    private Point X1;
    private Point X2;
    private RectangleBorder rectangleBorder;

    public void draw() {
        int fromIndex = canvas.indexOf(X1);
        int toIndex = canvas.indexOf(X2);
        if (fromIndex > toIndex) {
            fromIndex = canvas.indexOf(X2);
            toIndex = canvas.indexOf(X1);
        }
        List<Point> area = canvas.subList(fromIndex, toIndex + 1);
        drawLine(X1, X2, area, rectangleBorder);
    }

    private void drawVerticalLine(int x, int yUp, int yDown, List<Point> points) {
        if (yUp < yDown) {
            int temp = yDown;
            yDown = yUp;
            yUp = temp;
        }
        for (Point point : points) {
            if (point.getX() == x) {
                if (point.getY() <= yUp && point.getY() >= yDown) {
                    point.setElement(" x ");
                }
            }
        }
    }

    private void drawLine(Point A, Point B, List<Point> points, RectangleBorder rectangleBorder) {
        double epsilon = 0.5;
        if (A.getX() - B.getX() == 0) {
            drawVerticalLine(A.getX(), A.getY(), B.getY(), points);
        } else {
            //slope
            double slope = ((double) A.getY() - (double) B.getY()) / ((double) A.getX() - (double) B.getX());
            //y=slope * x + b
            double b = A.getY() - (slope * A.getX());

            //verify if point is on line
            points.forEach(point -> {
                if (Math.abs(slope * point.getX() + b - point.getY()) <= epsilon) {
                    point.setElement(rectangleBorder.getBorder());
                }
            });
        }
    }
}

