package com.home.drawing.shape;

import com.home.drawing.drawer.RectangleBorder;
import com.home.drawing.tools.Point;
import lombok.Builder;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

@Builder
public class Rectangle implements Shape {

    private Point upLeft;
    private Point loRight;
    private RectangleBorder rectangleBorder;
    private List<Point> canvas;

    @Override
    public void draw() {
        Point upRight = new Point(loRight.getX(), upLeft.getY());
        Point loLeft = new Point(upLeft.getX(), loRight.getY());
        drawLine(canvas, Pair.of(upLeft, upRight), rectangleBorder);
        drawLine(canvas, Pair.of(loLeft, loRight), rectangleBorder);
        drawLine(canvas, Pair.of(upLeft, loLeft), rectangleBorder);
        drawLine(canvas, Pair.of(upRight, loRight), rectangleBorder);
    }

    private void drawLine(List<Point> canvas, Pair<Point, Point> x1x2, RectangleBorder rectangleBorder) {
        Shape line = Line.builder()
                .canvas(canvas)
                .X1(x1x2.getLeft())
                .X2(x1x2.getRight())
                .rectangleBorder(rectangleBorder)
                .build();
        line.draw();
    }

}
